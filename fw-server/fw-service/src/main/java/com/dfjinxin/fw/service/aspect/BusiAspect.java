package com.dfjinxin.fw.service.aspect;

import com.dfjinxin.fw.service.utils.RemoteIpUtils;
import com.dfjinxin.fw.service.utils.SysJsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 业务日志，切面处理类
 */
@Aspect
@Component
@Slf4j
public class BusiAspect {

    @Pointcut("execution(* com.dfjinxin.fw.service.external.controller.*.*(..))")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        //执行方法
        Object result = point.proceed();
        //执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;

        //保存日志
        saveSysLog(point, result, time);

        return result;
    }

    private void saveSysLog(ProceedingJoinPoint joinPoint, Object result, long time) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        String params = SysJsonUtils.objectsToJson(joinPoint.getArgs());//请求参数
        BusiLog busiLog = new BusiLog().setIp(RemoteIpUtils.getCliectIp(request))
                .setRequestPra(params).setResponse(result)
                .setClassName(joinPoint.getSignature().getDeclaringTypeName())
                .setMethodName(joinPoint.getSignature().getName());

        log.info("接口日志：{}", busiLog);
    }
}
