package com.dfjinxin.fw.service.api.service.busi;

import com.dfjinxin.fw.service.api.constant.FwConstant;
import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxAnomalousInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxAnomalousInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import com.travelsky.dss.sc.nativeclient.msc.context.TokenStorageContext;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthException;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthTokenOfflineException;
import com.travelsky.dss.sc.nativeclient.msc.exception.RequestBusinessRuntimeException;
import com.travelsky.dss.sc.nativeclient.msc.exception.dto.BusinessExceptionInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取民航离岛旅客安检异常信息接口 某一天
 */
@Slf4j
@Service
public class Api31OneDayService implements RunService {

    @Autowired
    private TsgptZhxAnomalousInfoService tsgptZhxAnomalousInfoService;

    @Autowired
    private Api313Service api313Service;

    @Autowired
    private Api315Service api315Service;

    @Autowired
    private Api317Service api317Service;

    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {

        /**
         * pasName String 张三 旅客姓名
         * certType String NI 旅客证件类型
         * certNo String 12345678 旅客证件号
         */
        try {
            log.info("【获取民航离岛旅客安检异常信息接口】执行参数：{}", param);
            List<PassengerDTO> passengerAbnDTOList = SmpPsrResource.queryAbnormalSecInfo(param);
            if(null != passengerAbnDTOList && passengerAbnDTOList.size() > 0){
                log.info("接收总数：{}", passengerAbnDTOList.size());
                for(PassengerDTO dto : passengerAbnDTOList){

                    /**
                     * @return java.util.List<com.seaboxdata.auth.api.dto.OauthResultTenantDTO>
                     * @author makaiyu
                     * @description 获取全部租户信息
                     * @date 10:28 2019/5/29
                     **/
                    tsgptZhxAnomalousInfoService.createByDto(dto);

                    /**
                     * 获取今明后三天民航旅客进出岛订票信息接口
                     */
                    api313Service.run(dto.getCertNo());

                    /**
                     * 获取当日民航离岛旅客值机安检信息接口
                     */
                    api315Service.run(dto.getCertNo());

                    /**
                     * 获取今明后民航进出岛旅客同行人信息接口
                     */
                    api317Service.run(dto.getCertNo());
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取民航离岛旅客安检异常信息接口 某一天", e);
        }

    }
}
