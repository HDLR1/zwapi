/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.modules.task;

import com.dfjinxin.fw.service.api.service.busi.Api315Service;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取当日民航离岛旅客值机安检信息接口
 */
@Slf4j
@Component("api315Task")
public class Api315Task implements ITask {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Api315Service api315Service;

	@Override
	public void run(String params){
		logger.info("开始执行，参数为：{}", params);
		try {
			api315Service.run(params);
		}catch (Exception e){
			log.error("获取当日民航离岛旅客值机安检信息接口异常：", e);
		}

		logger.info("执行结束，参数为：{}", params);

	}
}
