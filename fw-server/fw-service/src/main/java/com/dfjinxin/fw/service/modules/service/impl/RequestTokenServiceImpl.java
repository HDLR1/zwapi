package com.dfjinxin.fw.service.modules.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dfjinxin.fw.service.modules.dao.RequestTokenDao;
import com.dfjinxin.fw.service.modules.entity.RequestToken;
import com.dfjinxin.fw.service.modules.service.RequestTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestTokenServiceImpl implements RequestTokenService {

    @Autowired
    private RequestTokenDao requestTokenDao;

    @Override
    public synchronized RequestToken queryToken(String systemId) {
        LambdaQueryWrapper<RequestToken> lambdaQueryWrapper = new QueryWrapper<RequestToken>().lambda();
        lambdaQueryWrapper.eq(RequestToken::getSystemId, systemId);
        List<RequestToken> requestTokens = requestTokenDao.selectList(lambdaQueryWrapper);
        if(!requestTokens.isEmpty() && requestTokens.size() > 0){
            requestTokens.get(0);
        }
        return null;
    }

    @Override
    public synchronized Boolean save(RequestToken requestToken) {
        /**
         * 删除老的token
         */
        LambdaQueryWrapper<RequestToken> lambdaQueryWrapper = new QueryWrapper<RequestToken>().lambda();
        lambdaQueryWrapper.eq(RequestToken::getSystemId, requestToken.getSystemId());
        requestTokenDao.delete(lambdaQueryWrapper);

        /**
         * 保存新的token
         */
        long nowTime = System.currentTimeMillis();
        requestToken.setMillisecondsCount(String.valueOf(nowTime));
        requestTokenDao.insert(requestToken);

        return true;
    }
}
