package com.dfjinxin.fw.service.config;

import com.dfjinxin.fw.service.api.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 工程启动后执行
 */
@Slf4j
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

    @Autowired
    private LoginService loginService;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("开始执行loginService.createToken()");
        try {
            loginService.createToken();
        }catch (Exception e) {
            log.error("执行loginService.createToken()异常：", e);
        }
        log.info("执行结束loginService.createToken()");

    }
}
