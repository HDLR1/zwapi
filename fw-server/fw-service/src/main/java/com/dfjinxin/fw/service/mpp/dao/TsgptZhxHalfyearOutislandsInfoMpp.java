package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearOutislandsInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 历史半年离岛旅客值机安检信息
 */
@Repository
@Mapper
public interface TsgptZhxHalfyearOutislandsInfoMpp extends BaseMapper<TsgptZhxHalfyearOutislandsInfo> {

}
