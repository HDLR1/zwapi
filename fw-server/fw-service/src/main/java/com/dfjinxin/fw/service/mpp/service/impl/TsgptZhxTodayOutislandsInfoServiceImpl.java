package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxTodayOutislandsInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxTodayOutislandsInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxTodayOutislandsInfoService;
import org.springframework.stereotype.Service;

/**
 * 今日离岛旅客值机安检信息
 */
@Service
public class TsgptZhxTodayOutislandsInfoServiceImpl extends ServiceImpl<TsgptZhxTodayOutislandsInfoMpp, TsgptZhxTodayOutislandsInfo> implements TsgptZhxTodayOutislandsInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxTodayOutislandsInfo info) {
        return this.save(info);
    }

}
