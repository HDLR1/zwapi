package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxAnomalousInfo;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;

/**
 * 旅客安检信息
 */
public interface TsgptZhxAnomalousInfoService extends IService<TsgptZhxAnomalousInfo> {

    boolean create(TsgptZhxAnomalousInfo info);

    boolean createByDto(PassengerDTO dto);
}
