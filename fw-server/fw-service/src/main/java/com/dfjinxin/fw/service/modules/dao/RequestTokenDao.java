/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.modules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.modules.entity.RequestToken;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 */
@Repository
@Mapper
public interface RequestTokenDao extends BaseMapper<RequestToken> {
	
}
