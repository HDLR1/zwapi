package com.dfjinxin.fw.service.external.controller;

import com.dfjinxin.fw.service.external.response.ZhxResponse;
import com.dfjinxin.fw.service.external.service.ApiService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxAnomalousInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ApiController {

    @Autowired
    private ApiService apiService;

    @PostMapping("/zhx/api/31/test")
    public ZhxResponse zhxApi31Test(){
        try {
            return ZhxResponse.ok().setData(new TsgptZhxAnomalousInfo().setCertno("1111111").setPasname("测试数据"));
        }catch (Exception e) {
            log.info("测试接口异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取民航离岛旅客安检异常信息接口 180天
     */
    @PostMapping("/zhx/api/31/all")
    public ZhxResponse zhxApi31All(){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi31AllOrOneDay(null));
        }catch (Exception e) {
            log.info("获取民航离岛旅客安检异常信息接口 180天异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取民航离岛旅客安检异常信息接口 某一天
     */
    @PostMapping("/zhx/api/31/oneDay")
    public ZhxResponse zhxApi31OneDay(@RequestParam("day")String day){
        try {
            if(StringUtils.isBlank(day)){
                return ZhxResponse.invalidExcep();
            }
            return ZhxResponse.ok().setData(apiService.zhxApi31AllOrOneDay(day));
        }catch (Exception e) {
            log.info("获取民航离岛旅客安检异常信息接口 180天异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取民航进出岛旅客低价高频信息接口
     */
    @PostMapping("/zhx/api/312")
    public ZhxResponse zhxApi312(@RequestParam("travelCount")int travelCount, @RequestParam("priceDiscount")int priceDiscount){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi312(travelCount,priceDiscount));
        }catch (Exception e) {
            log.info("获取民航进出岛旅客低价高频信息接口异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取今明后三天民航旅客进出岛订票信息接口
     */
    @PostMapping("/zhx/api/313")
    public ZhxResponse zhxApi313(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi313(certCode));
        }catch (Exception e) {
            log.info("获取今明后三天民航旅客进出岛订票信息接口异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取历史半年民航旅客进出岛订票信息接口
     */
    @PostMapping("/zhx/api/314")
    public ZhxResponse zhxApi314(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi314(certCode));
        }catch (Exception e) {
            log.info("异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取当日民航离岛旅客值机安检信息接口
     */
    @PostMapping("/zhx/api/315")
    public ZhxResponse zhxApi315(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi315(certCode));
        }catch (Exception e) {
            log.info("异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取历史半年民航离岛旅客值机安检信息接口
     */
    @PostMapping("/zhx/api/316")
    public ZhxResponse zhxApi316(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi316(certCode));
        }catch (Exception e) {
            log.info("异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取今明后民航进出岛旅客同行人信息接口
     */
    @PostMapping("/zhx/api/317")
    public ZhxResponse zhxApi317(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi317(certCode));
        }catch (Exception e) {
            log.info("异常: {}", e);
            return ZhxResponse.excep();
        }
    }

    /**
     * 获取历史半年民航进出岛旅客同行人信息接口
     */
    @PostMapping("/zhx/api/318")
    public ZhxResponse zhxApi318(@RequestParam("certCode")String certCode){
        try {
            return ZhxResponse.ok().setData(apiService.zhxApi318(certCode));
        }catch (Exception e) {
            log.info("异常: {}", e);
            return ZhxResponse.excep();
        }
    }

}
