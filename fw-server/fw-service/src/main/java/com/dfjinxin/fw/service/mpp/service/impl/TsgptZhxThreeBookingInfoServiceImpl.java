package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxThreeBookingInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeBookingInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxThreeBookingInfoService;
import org.springframework.stereotype.Service;

/**
 * 今明后三天进出岛旅客订票信息
 */
@Service
public class TsgptZhxThreeBookingInfoServiceImpl extends ServiceImpl<TsgptZhxThreeBookingInfoMpp, TsgptZhxThreeBookingInfo> implements TsgptZhxThreeBookingInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxThreeBookingInfo info) {
        return this.save(info);
    }

}
