package com.dfjinxin.fw.service.mpp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.EtTravelDTO;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 历史半年进出岛旅客订票信息
 */
@Data
@Accessors(chain = true)
@TableName("t_sgpt_zhx_halfyear_booking_info")
public class TsgptZhxHalfyearBookingInfo implements Serializable {

    public TsgptZhxHalfyearBookingInfo(){
        super();
    }

    public TsgptZhxHalfyearBookingInfo(EtTravelDTO dto){
        super();

        /**
         * '旅客姓名'
         */
        this.pasname = dto.getPasName();

        /**
         * '旅客证件类型'
         */
        this.certtype = dto.getCertType();

        /**
         * '旅客证件号码'
         */
        this.certno = dto.getCertNo();

        /**
         * '航班号'
         */
        this.fltnumber = dto.getFltNumber();

        /**
         * '航班日期'
         */
        this.fltdate = dto.getFltDate();

        /**
         * '航空公司'
         */
        this.airln = dto.getAirln();

        /**
         * '起始机场'
         */
        this.dptairport = dto.getDptAirport();

        /**
         * '起始城市'
         */
        this.dptcity = dto.getDptCity();

        /**
         * '到达机场'
         */
        this.arrairport = dto.getArrAirport();

        /**
         * '到达城市'
         */
        this.arrcity = dto.getArrCity();

        /**
         * '旅客电子客票号'
         */
        this.pastkne = dto.getPasTkne();

        /**
         * '出票时间'
         */
        this.issuetime = dto.getIssueTime();

        /**
         * '票价折扣'
         */
        if(null != dto.getPriceDiscount()){
            this.pricediscount = new Double(dto.getPriceDiscount()).intValue();
        }
    }

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * '旅客姓名'
     */
    private String pasname;

    /**
     * '旅客证件类型'
     */
    private String certtype;

    /**
     * '旅客证件号码'
     */
    private String certno;

    /**
     * '航班号'
     */
    private String fltnumber;

    /**
     * '航班日期'
     */
    private String fltdate;

    /**
     * '航空公司'
     */
    private String airln;

    /**
     * '起始机场'
     */
    private String dptairport;

    /**
     * '起始城市'
     */
    private String dptcity;

    /**
     * '到达机场'
     */
    private String arrairport;

    /**
     * '到达城市'
     */
    private String arrcity;

    /**
     * '旅客电子客票号'
     */
    private String pastkne;

    /**
     * '出票时间'
     */
    private String issuetime;

    /**
     * '票价折扣'
     */
    private Integer pricediscount;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createtm;

}
